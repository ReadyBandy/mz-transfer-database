#-*- coding: utf-8 -*- 

#this program requieres English version of managerzone while importing data and club membership

import requests
import time
import json
import databasesoccer
import time
from datetime import datetime
from datetime import timedelta
import logintomz

#global vars
login=''
password=''
cookie_soccer=''

def SGetCookie():  
    """getting 'cookie' from managerzone.com'"""
    global cookie_soccer
    cookie_soccer=logintomz.SoccerGetSSID(login, password) #must have to download data, separately for both sports including paid membership for training graph

def DownloadAndClearListSoccer(): 
    """downloading transfer list, clearing from unnecessary html, diving string to list """
    url = "https://www.managerzone.com/ajax.php"

    querystring = {"p":"transfer","sub":"transfer-search","sport":"soccer","issearch":"true","u":"","nationality":"all_nationalities","deadline":"0","category":"","valuea":"","valueb":"","bida":"","bidb":"","agea":"19","ageb":"37","birth_season_low":"32","birth_season_high":"50","tot_low":"0","tot_high":"110","s0a":"0","s0b":"10","s1a":"0","s1b":"10","s2a":"0","s2b":"10","s3a":"0","s3b":"10","s4a":"0","s4b":"10","s5a":"0","s5b":"10","s6a":"0","s6b":"10","s7a":"0","s7b":"10","s8a":"0","s8b":"10","s9a":"0","s9b":"10","s10a":"0","s10b":"10","s11a":"0","s11b":"10","s12a":"0","s12b":"10","o":"0"}

    payload = ""
    headers = {
        'Cookie': cookie_soccer,
        'cache-control': "no-cache",
        'Postman-Token': "d30716e0-0495-4510-9cda-12783bf5528e"
        }

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
    
    code_from_transferlist=response.text
    initial_list=code_from_transferlist.split("thePlayers_")
    del initial_list[0]  #removing information about manager's team (here: manager==owner of account)


    list_of_unwanted_symbols=['\\" class=\\"playerContainer\\"><h2 class=\\"subheader clearfix\\"><div title=\\"','\\">\\t\\t<a href=\\"\\/?p=players&pid=', '&tid=', '\\" class=\\"subheader\\">\\n\\t\\t\\t<img title=\\"',
        '\\" src=\\"nocache-693\\/img\\/flags\\/15\\/', '.png\\" style=\\"vertical-align: middle\\">&nbsp;&nbsp;<span class=\\"player_name\\">','<\\/span>\\n\\t\\t<\\/a>\\n\\t\\t\\n\\t\\t\\n\\t\\t\\n\\t\\t<span id=\\"player_id_',
        '\\" class=\\"floatRight\\">id: <span class=\\"player_id_span\\">','<\\/span><\\/span><\\/div><\\/h2><div class=\\"mainContent\\" style=\\"padding: 5px;\\"><a name=\\"',
        '\\"><\\/a>\\n<div class=\\"clearfix\\">\\n\\t<div class=\\"floatLeft\\" style=\\"width: 300px\\">\\n\\t\\t<table cellpadding=0 cellspacing=0 width=\\"100%\\" border=0>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=top width=\\"100\\">\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\"player-jersey\\">\\n\\t\\t\\t<div style=\\"background-image: url(dynimg\\/garment.php\\/n=a\\/set=home\\/pi=shirt\\/pa=',
        '<\\/span>\\n\\t\\t<\\/a>\\n\\t\\t\\n\\t\\t\\n\\t\\t', ';\\">\\n\\t\\t\\t\\t<span>', '<\\/span>\\n\\t\\t\\t<\\/div>\\n\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t\\t<td style=\\"padding: 3px 3px 3px 3px;\\">\\n\\t\\t\\t\\t\\t<table cellpadding=\\"0\\" cellspacing=\\"0\\" width=\\"100%\\">\\n\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t\\t<td>',
        '<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td><strong>','<\\/strong><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t\\t<td>','<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td><strong>','<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td><strong>','<\\/strong><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t\\t<td title=\\"All skills combined except Form and Experience\\">',
        '<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td valign=\\"top\\"><strong>', 
        '<\\/strong>&nbsp;\\t\\t<span class=\\"help_button_placeholder\\">\\n\\t\\t\\t<a class=\\"help_button\\" href=\\"#\\" onclick=\\"showHelpLayer', "('All+skills+combined+except+Form+and+Experience', 'Total Skill Balls', true); return false\\", 
        '>\\n\\t\\t\\t\\t<span class=\\"help_button_wrapper\\">\\n\\t\\t\\t\\t\\t<span class=\\"help_button_text\\">?<\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t<\\/table>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr style=\\"height: 25px;\\"><\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td colspan=\\"2\\">\\t\\t<table border=\\"0\\" cellpadding=\\"0\\" cellspacing=\\"0\\" class=\\"player_skills\\">\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">',
        'Speed<\\/span><span style=\\"display: none\\">Sp<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        '\\" class=\\"skill\\" width=\\"126\\" height=\\"10\\" alt=\\"','\\" \\/><\\/td>\\n\\t\\t\\t\\t<td class=\\"skillval\\">(',')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Stamina<\\/span><span style=\\"display: none\\">St<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/','\\" class=\\"skill\\" width=\\"126\\" height=\\"10\\" alt=\\"',
        'wlevel_1.gif','wlevel_2.gif','wlevel_3.gif','wlevel_4.gif','wlevel_5.gif','wlevel_6.gif','wlevel_7.gif','wlevel_8.gif','wlevel_9.gif','wlevel_10.gif','nocache-695',')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Play Intelligence<\\/span><span style=\\"display: none\\">PI<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/ \\" class=\\"skill\\" width=\\"126\\" height=\\"10\\" alt=\\"',
        '\\" \\/><\\/td>\\n\\t\\t\\t\\t<td class=\\"skillval\\">(',')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Play Intelligence<\\/span><span style=\\"display: none\\">PI<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Passing<\\/span><span style=\\"display: none\\">Pa<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/', ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Shooting<\\/span><span style=\\"display: none\\">Sh<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Heading<\\/span><span style=\\"display: none\\">He<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/', ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Keeping<\\/span><span style=\\"display: none\\">Ke<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Ball Control<\\/span><span style=\\"display: none\\">BC<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/', ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Tackling<\\/span><span style=\\"display: none\\">Ta<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Aerial Passing<\\/span><span style=\\"display: none\\">AP<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/', 'wlevel_0.gif', ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Set Plays<\\/span><span style=\\"display: none\\">SP<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Experience<\\/span><span style=\\"display: none\\">Ex<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/', ')<\\/td><\\/tr>\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td><span class=\\"clippable\\">Form<\\/span><span style=\\"display: none\\">Fo<\\/span><\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td width=\\"7\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td', 'width=\\"6\\">&nbsp;<\\/td>\\n\\t\\t\\t\\t<td><img src=\\"nocache-694\\/img\\/soccer\\/',
        ')<\\/td><\\/tr><\\/table><\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\t<div class=\\"floatRight transfer-control-area\\">\\n\\t\\t<div style=\\"margin-top: 3px\\">\\n\\n\\t<div class=\\"box_dark\\" style=\\"height: 6em; padding: 3px 6px\\">\\n\\t\\t<table width=\\"100%\\" cellspacing=0 cellpadding=0>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td colspan=2 valign=top>\\n\\t\\t\\t\\t\\t<table cellpadding=0 cellspacing=0 width=100%>\\n\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t\\t<td>Value:<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td><span title=\\"', '&nbsp;', '\\" src=\\"nocache-694\\/img\\/flags\\/15\\/','\\" class=\\"bold\\">','<\\/span><\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td>',
        ': <span title=\\"','\\" class=\\"bold\\">','<\\/span><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t<\\/table>\\n\\t\\t\\t\\t\\t<br>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr><td>Club<\\/td><td><strong style=\\"max-width: 125px\\" class=\\"clippable\\"><a href=\\"\\/?p=','\\">','<\\/a><\\/strong><\\/td><\\/tr>\\n\\t\\t\\t<tr><td>','<\\/td><td><strong>','<\\/strong><\\/td><\\/tr>\\n\\t\\t\\t<tr><td>','<\\/td><td title=\\"','<strong>',
        '<\\/strong><\\/td><\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n\\t<div class=\\"box_dark\\" style=\\"margin-top: 6px; min-height: 1em; padding: 3px 6px \\n\\t\\t<table width=\\"100%\\" cellspacing=0 cellpadding=0 height=\\"100% \\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td align=left valign=top>\\n\\t\\t\\t\\t\\t<table width=\\"100%\\" cellpadding=0 cellspacing=0>\\n\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t\\t<td width=\\"80\\" class=\\"clippable',':<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td title=\\"',':<\\/td>\\n\\t\\t\\t\\t\\t\\t\\t<td title=\\"',
        '<\\/strong><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t<\\/table>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t\\t<td valign=middle align=right>\\n\\t\\t\\t\\t\\t\\t\\t<span class=\\"player_icon_placeholder bid_button \\n\\t\\t\\t<a href=\\"javascript: buy(',');\\" title=\\"Place bid\\" class=\\"player_icon \\n\\t\\t\\t\\t\\n\\t\\t\\t\\t<span class=\\"player_icon_wrapper \\n\\t\\t\\t\\t\\t<span class=\\"player_icon_image\\" style=\\"background-image: url(nocache-694\\/img\\/player\\/sell.png) <\\/span>\\n\\t\\t\\t\\t\\t<span class=\\"player_icon_text <\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span>\\t\\t<span class=\\"player_icon_placeholder monitor_add_link \\n\\t\\t\\t<a href=\\"\\/?p=players&sub=monitor&pid=',
        '&extra=transfer\\" title=\\"Shortlist\\" class=\\"player_icon \\n\\t\\t\\t\\t\\n\\t\\t\\t\\t<span class=\\"player_icon_wrapper \\n\\t\\t\\t\\t\\t<span class=\\"player_icon_image\\" style=\\"background-image: url(nocache-694\\/img\\/player\\/monitor.png)','<\\/span>\\n\\t\\t\\t\\t\\t<span class=\\"player_icon_text <\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span>\\t\\t<span class=\\"player_icon_placeholder training_graphs \\n\\t\\t\\t<a href=\\"\\/',
        '\\" title=\\"Training Graphs\\" class=\\"player_icon \\n\\t\\t\\t\\t<img src=\\"nocache-694\\/img\\/c_12x12_for_buttons.gif\\" class=\\"cm_icon\\" alt=\\"\\" \\/>\\n\\t\\t\\t\\t<span class=\\"player_icon_wrapper \\n\\t\\t\\t\\t\\t<span class=\\"player_icon_image\\" style=\\"background-image: url(nocache-694\\/img\\/player\\/transfer_training.png) <\\/span>\\n\\t\\t\\t\\t\\t<span class=\\"player_icon_text <\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n\\t<div class=\\"box_dark\\" style=\\"margin-top: 6px; height: 150px; padding: 3px \\n\\t\\t<table width=\\"100%\\" height=\\"100% \\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=\\"top\\" style=\\"height: 15px; Bid history:<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=top>\\n\\t\\t\\t\\t\\t<div style=\\"height: 70px; overflow: auto \\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px; \\t\\t\\t\\t\\t<span class=\\"dual_nationalities_list_title'
        '\\" title=\\"Training Graphs\\" class=\\"player_icon \\n\\t\\t\\t\\t<img src=\\"nocache-694\\/img\\/c_12x12_for_buttons.gif\\" class=\\"cm_icon\\" alt=\\"\\"','\\/>\\n\\t\\t\\t\\t<span class=\\"player_icon_wrapper \\n\\t\\t\\t\\t\\t<span class=\\"player_icon_image\\" style=\\"background-image: url(nocache-694\\/img\\/player\\/transfer_training.png) <\\/span>\\n\\t\\t\\t\\t\\t<span class=\\"player_icon_text <\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n\\t<div class=\\"box_dark\\" style=\\"margin-top: 6px; height: 150px; padding: 3px \\n\\t\\t<table width=\\"100%\\" height=\\"100% \\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=\\"top\\" style=\\"height: 15px;',
        ':<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=top>\\n\\t\\t\\t\\t\\t<div style=\\"height: 70px; overflow: auto \\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px; <\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div><div id=\\"',
        '<\\/strong><\\/td>\\n\\t\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<tr>\\n\\t\\t\\t\\t\\t\\t<td colspan=\\"2 <a href=\\"\\/?p=team', '<\\/a><br>(',')<\\/td>\\n\\t\\t\\t\\t\\t<\\/tr>\\n\\t\\t\\t\\t\\t<\\/table>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t\\t<td valign=middle align=right>\\n\\t\\t\\t\\t\\t\\t\\t<span class=\\"player_icon_placeholder bid_button \\n\\t\\t\\t<a href=\\"javascript: buy(', '\\" title=\\"Training Graphs\\" class=\\"player_icon \\n\\t\\t\\t\\t<img src=\\"nocache-694\\/img\\/c_12x12_for_buttons.gif\\" class=\\"cm_icon\\" alt=\\"\\"',
        ':<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=top>\\n\\t\\t\\t\\t\\t<div style=\\"height: 70px; overflow: auto \\n\\t\\t\\t\\t\\t\\t<span style=\\"max-width: 260px\\" class=\\"clippable <span class=\\"bold\\" title=\\"', '<\\/span>, <a href=\\"\\/?p=team','<\\/a><\\/span>\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px; \\t\\t\\t\\t\\t<span', 'class=\\"dual_nationalities_list_title',':<\\/span>\\n\\t\\t\\t\\t\\t<div class=\\"dual_nationalities_list_container clearfix',
        '\\n\\t\\t\\t\\t\\t\\t<div class=\\"dual_navigation',' <a class=\\"prev disabled\\" href=\\"#','<span>&lt;<\\/span><\\/a><\\/div>\\n\\t\\t\\t\\t\\t\\t<div','class=\\"dual_nationalities_list_wrapper clearfix \\n\\t\\t\\t\\t\\t\\t\\t<dl','class=\\"dual_nationalities_list <dd title=\\"','\\" class=\\"valid','<img src=\\"nocache-694\\/img\\/flags\\/15\\/','\\" alt=\\"','<span>', ':<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=top>\\n\\t\\t\\t\\t\\t<div style=\\"height: 70px; overflow: auto', '\\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px; \\t\\t\\t\\t\\t<span','<\\/span><\\/dd>\\t\\t\\t\\t\\t\\t\\t\\t<\\/dl>\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>',
        '<a class=\\"next disabled\\" href=\\"#','&gt;<\\/span><\\/a><\\/div>\\n\\t\\t\\t\\t\\t<\\/div><\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div><div id=\\"','<\\/a><\\/span><span style=\\"max-width: 260px\\" class=\\"clippable','<span class=\\"bold\\" title=\\"','<\\/a><\\/span><span style=\\"max-width: 260px\\" class=\\"clippable <span class=\\"bold\\" title=\\"','<\\/span><\\/dd><dd title=\\"','<\\/span><\\/dd><dd title=\\"','<\\/a><\\/span>\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px;','<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div><div id=\\"',
        '<\\/span>\\n\\t\\t\\t\\t\\t<span ', 'class=\\"player_icon_text ','<\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/a>\\n\\t\\t<\\/span>\\t\\t<span','class=\\"player_icon_placeholder','semi_transparent \\n\\t\\t\\t<span title=\\"Training Graphs\\" class=\\"player_icon \\n\\t\\t\\t\\t<img src=\\"nocache-694\\/img\\/c_12x12_for_buttons.gif\\"','‘;’','class=\\"cm_icon \\" \\/>\\n\\t\\t\\t\\t<span class=\\"player_icon_wrapper \\n\\t\\t\\t\\t\\t<span','class=\\"player_icon_image\\" style=\\"background-image: url(nocache-694\\/img\\/player\\/transfer_training.png) <\\/span>\\n\\t\\t\\t\\t\\t<span',';class=\\"player_icon_text','<\\/span>\\n\\t\\t\\t\\t<\\/span>\\n\\t\\t\\t<\\/span>\\n\\t\\t<\\/span>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n\\t<div class=\\"box_dark\\" style=\\"margin-top: 6px; height: 150px; padding: 3px \\n\\t\\t<table width=\\"100%\\" height=\\"100% \\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td valign=\\"top\\" style=\\"height: 15px;',
        '<\\/a><\\/span>\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px;','<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div><div id=\\"','\\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t<\\/div>\\n\\t\\t\\t\\t<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t\\t<tr>\\n\\t\\t\\t\\t<td style=\\"height: 15px;','<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div>","monitor_search":{"my_players":{},"bid_players":{}}}','style=\\"background-image:','class=\\"player_icon_image\\"','url(nocache-694\\/img\\/player\\/transfer_training.png)','<a class=\\"next\\" href=\\"#',
        '  ','"','   ','(',')','    ','     ','  ',
        '<span class=\\','special_player','<\\/span><\\/a>\\n\\t\\t<span','id=\\','player_id_','&gt;<\\/span><\\/a><\\/div>\\n\\t\\t\\t\\t\\t<\\/div>','<a href=\\','\\/?p=national_teams&type=u21&sub=statistics&player_search=',
        '<\\/td>\\n\\t\\t\\t<\\/tr>\\n\\t\\t<\\/table>\\n\\t<\\/div>\\n\\n<\\/div>\\n\\t<\\/div>\\n<\\/div><\\/div><\\/div>', 'monitor_search',':{ my_players',':{ count :','players :','\\t\\t<div class=\\ transfer_monitor_player\\','title=\\ Sell:','\\n\\t\\t\\t<img','\\ src=\\','nocache-694\\/img\\/flags\\/10\\/','\\/','title=\\','\\n\\t\\t\\t<span','style=\\','lo=', 'no=', 'sp=soccer',
        '    ','    ',
        '< span>','<span display: ','< td>','>\\n\\t\\t\\t\\t<td width=\\ ','<span display: none','\\n\\t\\t\\t\\t','<span','display:','none ','<td width=\\','img','soccer','<td>','<src=\\','< tr>\\t\\t\\t<tr>','clippable','SpeedSp','StaminaSt','6<src=\\','width=\\','Place bid\\',';\\','class=\\',
        'Sp     7     7     6    <  src=\\']
        
    for iterator in range(20):
        for symbol in list_of_unwanted_symbols:
            initial_list[iterator]=initial_list[iterator].replace(symbol, ' ')

    return initial_list

def GetTeamCountrySoccer(team_id): 
    """getting buyer's country name"""

    link='https://www.managerzone.com/?p=team&tid='+team_id

    url = link
    querystring = ''
    payload = ""
    headers = {
        'Cookie': cookie_soccer,
        'cache-control': "no-cache",
        'Postman-Token': "d30716e0-0495-4510-9cda-12783bf5528e"
        }
    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
    html=response.text.split()
    index=html.index('clippable">League:</span>')
    country=html[index+3].replace('alt="','').replace('"','') #always on 3th item )after index
    list_of_countries=['Czech', 'Dominican', 'Northern','MZ', 'Saudi','Faroe','Costa','El','South']
    if country in list_of_countries:
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
    elif country=='Trinidad':
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
        index=index+1
        country=country+' '+html[index+3]
    elif (country=='United' and html[index+4].replace('alt="','').replace('"','')=='Arab'):
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
    elif (country=='United'):
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
    elif (country=='Bosnia'):
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
        index=index+1
        country=country+' '+html[index+3].replace('alt="','').replace('"','')
    return country

def PlayerStatsSoccer(number, initial_list):
    """#extracting player's stats from intial list from DownloadAndClearListSoccer(), returningd dictionary with player's stats"""

    #eliminating number of player from MZ request
    player_list=initial_list[number].split()
    player_list=player_list[1:]
    player_dict={}

    #getting index of ID in player list
    for element in range(10):
        if player_list[element].isnumeric():
            index = element
            break

    #setting string for player_name, first part should be always on 0 position
    player_name=player_list[0]


    #getting name string from multiple 'parts' of  player's name 
    for element in range(1,index):
        player_name=player_name+' '+ player_list[element]

    player_dict['Name']=player_name

    player_id=player_list[index]

    player_dict['PlayerID']=player_id

    #changing index to date position in list
    index=index+3

    player_date=player_list[index]

    player_dict['Date']=player_date

    #getting date of transfer of season
    date_of_transfer=datetime.strptime(player_date, "%d-%m-%Y") 
    ref_season=datetime.strptime('15-10-2013', "%d-%m-%Y") # 15-10-2013 #1st day of 48th season, used as referal 
    transfer_season=((date_of_transfer-ref_season).days)/91+48
    player_dict['TransferSeason']=int(transfer_season)
    

    #getting day of season
    player_dict['DayOfSeason']=DayOfSeason(player_dict)

    #getting date without info about year
    player_datedm=player_date[:5]
    player_dict['DateDM']=player_datedm

    #changing index to hour position in list
    index=index+1

    player_hour=player_list[index]

    player_dict['Hour']=player_hour

    #changing index to team id position in list
    index=index+2

    player_team=player_list[index]

    player_dict['TeamID']=player_team

    #getting seller team country
    player_dict['TeamCountry']=GetTeamCountrySoccer(player_team)

    #changing index to nationality position in list
    index=index+1

    player_nationality=player_list[index]

    #solution to problem that there are 2 (or 3) word name countries
    list_of_countries=['Czech', 'Dominican', 'Northern','MZ', 'Saudi','Faroe','Costa','El','South']
    if player_nationality in list_of_countries:
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
    elif player_nationality=='Trinidad':
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
    elif (player_nationality=='United' and player_list[index+1]=='Arab'):
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
    elif (player_nationality=='United'):
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
    elif (player_nationality=='Bosnia'):
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]
        index=index+1
        player_nationality=player_nationality+' '+player_list[index]

    player_dict['Nationality']=player_nationality

    #checking if player is in national team
    if 'National' in player_list:
        player_national_team = 1
    elif ('Player'+player_id) in player_list:
        player_national_team = 1
    else: 
        player_national_team = 0

    player_dict['NationalTeam']=player_national_team

    #index becomes number which is after postion of 'Season' string in list, it's useful to skip 'dynamic'variables
    index=player_list.index('Season')+1
    player_list=player_list[index:]

    #reseting index
    index=0

    player_born_in_season=player_list[index]

    player_dict['Season']=player_born_in_season

    #getting player's age (while transfer was made)
    player_dict['Age']=int(transfer_season)-int(player_born_in_season)


    # #changing index to position of total skill balls
    index=player_list.index('Balls:')
    player_total_balls=player_list[index+1]
    player_dict['TotalBalls']=player_total_balls

    # #changing index to position of speed
    index=player_list.index('Speed:')
    player_speed=player_list[index+1]
    player_dict['Speed']=player_speed


    #changing index to position of stamina
    index=player_list.index('Stamina:')
    player_stamina=player_list[index+1]
    player_dict['Stamina']=player_stamina

    #changing index to position of player intelligence
    index=player_list.index('Intelligence:')
    player_player_int=player_list[index+1]
    player_dict['PlayerIntelligence']=player_player_int

    #changing index to position of passing
    index=player_list.index('Passing:')
    player_passing=player_list[index+1]
    player_dict['Passing']=player_passing

    #changing index to position of shooting
    index=player_list.index('Shooting:')
    player_shooting=player_list[index+1]
    player_dict['Shooting']=player_shooting

    #changing index to position of heading
    index=player_list.index('Heading:')
    player_heading=player_list[index+1]
    player_dict['Heading']=player_heading

    #changing index to position of keeping
    index=player_list.index('Keeping:')
    player_keeping=player_list[index+1]
    player_dict['Keeping']=player_keeping

    #changing index to position of ball control
    index=player_list.index('Control:')
    player_ball_control=player_list[index+1]
    player_dict['BallControl']=player_ball_control

    #changing index to position of tackling
    index=player_list.index('Tackling:')
    player_tackling=player_list[index+1]
    player_dict['Tackling']=player_tackling

    #changing index to position of aerial passing, not to confuse with 'Passing'
    player_list=player_list[index:]
    index=player_list.index('Passing:')
    player_aerial_pass=player_list[index+1]
    player_dict['AerialPass']=player_aerial_pass

    #changing index to position of set plays
    index=player_list.index('Plays:')
    player_set_plays=player_list[index+1]
    player_dict['SetPlays']=player_set_plays

    #changing index to position of experience
    index=player_list.index('Experience:')
    player_experience=player_list[index+1]
    player_dict['Experience']=player_experience

    #changing index to position of form
    index=player_list.index('Form:')
    player_form=player_list[index+1]
    player_dict['Form']=player_form

    #cutting list to get value of player
    player_list=player_list[index+2:]

    #getting index of EUR value of player in player list
    index=player_list.index('EUR')

    #getting value from various items from list(number of them usally differs)
    value_list=[]

    for element in range(index-1, -1, -1):
        if player_list[element].isnumeric():
            value_list.append(player_list[element])
        else:
            break

    value_list.reverse()
    player_value=''.join(value_list)
    player_dict['Value']=player_value

    #cutting string to find salary
    index=player_list.index('Salary')

    player_list=player_list[index+1:]

    #getting index of EUR salary of player in player list
    index=player_list.index('EUR')

    #getting value from various items from list(number of them usally differs)
    salary_list=[]

    for element in range(index-1, -1, -1):
        if player_list[element].isnumeric():
            salary_list.append(player_list[element])
        else:
            break

    salary_list.reverse()
    player_salary=''.join(salary_list)
    player_dict['Salary']=player_salary

    #cutting string to find team name
    index=player_list.index(player_team)

    player_list=player_list[index+1:]
    deadline_index=player_list.index('Deadline')
    player_team_name=player_list[0]

    for element in range(1,deadline_index):
        player_team_name=player_team_name+" "+player_list[element]

    player_dict['TeamName']=player_team_name

    index=player_list.index('Price')
    player_list=player_list[index+1:]

    #getting index of EUR salary of player in player list
    index=player_list.index('EUR')

    #getting value from various items from list(number of them usally differs)
    asking_list=[]

    for element in range(index-1, -1, -1):
        if player_list[element].isnumeric():
            asking_list.append(player_list[element])
        else:
            break

    asking_list.reverse()
    player_asking_price=''.join(asking_list)

    player_dict['AskingPrice']=player_asking_price

    #cutting string to find latest bid in EUR
    index=player_list.index('Bid')

    player_list=player_list[index+1:]

    #getting index of EUR salary of player in player list
    index=player_list.index('EUR')

    #getting value from various items from list(number of them usally differs)
    bid_list=[]

    for element in range(index-1, -1, -1):
        if player_list[element].isnumeric():
            bid_list.append(player_list[element])
        else:
            break

    bid_list.reverse()
    player_latest_bid=''.join(bid_list)
    player_dict['LatestBid']=player_latest_bid

    player_dict['BuyersID']=0
    player_dict['BuyersTeamName']=''
    player_dict['BuyersCountry']=''
    #getting buyer's team_id
    if int(player_latest_bid)!=0:
        buy_id_index=player_list.index(player_id)-1
        buyers_id=player_list[buy_id_index]
        player_dict['BuyersID']=buyers_id
        tid_index=player_list.index(buyers_id)
        buyer_team_name=player_list[tid_index+1]
        for element in range(tid_index+2,buy_id_index):
            buyer_team_name=buyer_team_name+" "+player_list[element]
        player_dict['BuyersTeamName']=buyer_team_name
        player_dict['BuyersCountry']=GetTeamCountrySoccer(buyers_id)
        player_dict['BuyersID']=buyers_id
  
    #downloading info of maxed skill from training graph
    training_dict=TrainingGraphSoccer(player_id, player_dict['TransferSeason'])
    player_dict.update(training_dict)
    
    return player_dict

def SkillNumberMaxSoccer(skill):
    """checking if skill is maxed by passing list of strings of maxed skills """

    if skill=='"y":1':
        max = 'Speed'
    if skill=='"y":2':
        max = 'Stamina'
    if skill=='"y":3':
        max = 'Play Intelligence'
    if skill=='"y":4':
        max = 'Passing'
    if skill=='"y":5':
        max = 'Shooting'
    if skill=='"y":6':
        max = 'Heading'
    if skill=='"y":7':
        max = 'Keeping'
    if skill=='"y":8':
        max = 'Ball Control'
    if skill=='"y":9':
        max = 'Tackling'
    if skill=='"y":10':
        max = 'Aerial Passing'
    if skill=='"y":11':
        max = 'Set Plays'
    return max

def MaxedFilterSoccer(training_list):
    """#functionused by traininggraph"""
    maxed_list=[]
    number_of_max=training_list.count('"name":"Maxed"')
    for max in range(number_of_max):
        index=training_list.index('"name":"Maxed"')
        maxed_list.append(SkillNumberMaxSoccer(training_list[index-1]))
        training_list=training_list[index+1:]
    return maxed_list

def SeasonofLastCamp(timestamp_not_parsed):
    """Function is returning season of last training camp in which player was"""
    if len(timestamp_not_parsed)==18:
        timestamp=int(timestamp_not_parsed[5:15]) #striping from {"x": and  last '000', result in UNIX timestamp
    elif len(timestamp_not_parsed)==26:
        timestamp=int(timestamp_not_parsed[13:23])
    else:
        return 0
    date_last_camp=datetime.utcfromtimestamp(timestamp).strftime("%d-%m-%Y")
    dl_camp_parsed=datetime.strptime(date_last_camp, "%d-%m-%Y")
    ref_season=datetime.strptime('15-10-2013', "%d-%m-%Y") # 15-10-2013 1381795200 #1st day of 48th season UNIX timestamp, used as referal season
    season=((dl_camp_parsed-ref_season).days)/91+48
    return int(season)

def TrainingGraphSoccer(player_id,season): 
    """returns player dict, if skill==2 means player is maxed, ==1 player has no training graph, 0== unmaxed"""
    url = "https://www.managerzone.com/ajax.php"

    querystring = {"p":"trainingGraph","sub":"getJsonTrainingHistory","sport":"soccer","player_id":player_id}

    payload = ""
    headers = {
        'Cookie': cookie_soccer,
        'cache-control': "no-cache",
        'Postman-Token': "74bc2a57-9672-4fda-b6fc-041921ba4d24"
        }

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
    training_graph = response.text
    training_list=training_graph.split(',')

    seas_last_camp=0
    for item in range(len(training_list)):
        if training_list[item].find('Package')!=-1: #finding pos of training camp named eg. attacker package no 1
            if training_list[item-3]=='"marker":{"enabled":false}}': #some issue, not inspected yet, maybe TODO 
                continue
            seas_last_camp=SeasonofLastCamp(training_list[item-3])
    if  seas_last_camp==season:
        seas_last_camp=2
    else:
        seas_last_camp=0

    training_dict={}

    training_dict['CampInThisSeason']=seas_last_camp #0 for no camp, 1 for no graph, 2 for camp
    maxed_list=MaxedFilterSoccer(training_list)
    
    if 'Speed' in maxed_list:
        training_dict['SpeedMax']=2
    else:
        training_dict['SpeedMax']=0
    if 'Stamina' in maxed_list:
        training_dict['StaminaMax']=2
    else:
        training_dict['StaminaMax']=0
    if 'PlayIntelligence' in maxed_list:
        training_dict['PlayIntMax']=2
    else:
        training_dict['PlayIntMax']=0
    if 'Passing' in maxed_list:
        training_dict['PassingMax']=2
    else:
        training_dict['PassingMax']=0
    if 'Shooting' in maxed_list:
        training_dict['ShootingMax']=2
    else:
        training_dict['ShootingMax']=0
    if 'Heading' in maxed_list:
        training_dict['HeadingMax']=2
    else:
        training_dict['HeadingMax']=0
    if 'Keeping' in maxed_list:
        training_dict['KeepingMax']=2
    else:
        training_dict['KeepingMax']=0
    if 'Ball Control' in maxed_list:
        training_dict['BallControlMax']=2
    else:
        training_dict['BallControlMax']=0
    if 'Tackling' in maxed_list:
        training_dict['TacklingMax']=2
    else:
        training_dict['TacklingMax']=0
    if 'Aerial Passing' in maxed_list:
        training_dict['AerialPassMax']=2
    else:
        training_dict['AerialPassMax']=0
    if 'Set Plays' in maxed_list:
        training_dict['SetPlaysMax']=2
    else:
        training_dict['SetPlaysMax']=0

    #when no training graph
    if len(response.text)==0 or response.text=='An error occurred: You need to be logged in' or response.text=='There is no file here':
        training_dict['SpeedMax']=1
        training_dict['StaminaMax']=1
        training_dict['PlayIntMax']=1
        training_dict['PassingMax']=1
        training_dict['ShootingMax']=1
        training_dict['HeadingMax']=1
        training_dict['KeepingMax']=1
        training_dict['BallControlMax']=1
        training_dict['TacklingMax']=1
        training_dict['AerialPassMax']=1
        training_dict['SetPlaysMax']=1
        training_dict['IsTrainingGraph']=0
        training_dict['CampInThisSeason']=1
    else:
        training_dict['IsTrainingGraph']=1

    return training_dict

def hours_between(old_day, currenct_day):
    """counting hours"""
    o_day = datetime.strptime(old_day, "%d-%m-%Y %H:%M")
    c_day = datetime.strptime(currenct_day, "%d-%m-%Y %H:%M")
    hours=(c_day - o_day).total_seconds()/3600
    return int(hours)

def DayOfSeason(playerdictionary):
    """counting present day of season"""
    first_day=datetime.strptime('08-01-2019', "%d-%m-%Y")
    current_day= datetime.strptime(playerdictionary['Date'], "%d-%m-%Y")
    day_of_season=(current_day-first_day).days % 91 + 1
    return day_of_season

def DateDiff(playerdictionary):
    """counting diffrence in days, returns hours, intended to use to check if is it diffrent transfer """
    old_day=(databasesoccer.ReadDateAndHourfromDB(playerdictionary))
    current_day=playerdictionary['Date']+" "+playerdictionary['Hour']
    diff=hours_between(old_day, current_day)
    return diff

def SoccerMainFunc():
    """mainloop of this file"""
    initial_list=DownloadAndClearListSoccer()
    for player in range(20):
        player_dict=PlayerStatsSoccer(player,initial_list)
        if databasesoccer.ReadLatestBidfromDB(player_dict) == 'New player':
            databasesoccer.InsertData(player_dict)
        elif DateDiff(player_dict) > 23: #23 due to update where you can put player on transfer list immidately, may cause possible problems when transfers are postponed
            databasesoccer.InsertData(player_dict)
        elif (databasesoccer.ReadLatestBidfromDB(player_dict) != int(player_dict['LatestBid'])) :
            databasesoccer.theCursor = databasesoccer.db_conn.cursor()
            databasesoccer.theCursor.execute('UPDATE Players SET  Date=?, DateDM=?,  TransferSeason=?, DayOfSeason=?, Hour=?, CampInThisSeason=?, Age=?, BuyersTeamName=?, BuyersID=?, BuyersCountry=?,LatestBid=? WHERE PlayerID=? AND ID IN (SELECT ID FROM Players ORDER BY ID DESC LIMIT 1)',(player_dict['Date'], player_dict['DateDM'], player_dict['TransferSeason'], player_dict['DayOfSeason'], player_dict['Hour'], player_dict['CampInThisSeason'], player_dict['Age'], player_dict['BuyersTeamName'],player_dict['BuyersID'], player_dict['BuyersCountry'], player_dict['LatestBid'], player_dict['PlayerID']) )
            databasesoccer.db_conn.commit()
            databasesoccer.theCursor.close()

