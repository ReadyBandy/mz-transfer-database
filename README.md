# MZ transfer database

This project is intended to download information about transfers in Managerzone.com
You need to have Managerzone club membership in sport if you want download data.
By default, it's both football and hockey.

**Usage:**

Fill login and password to your MZ account in quoutes (as a string) in following lines:

`lgin=''`   
`pswd=''` 


in file [mz.py](https://gitlab.com/ReadyBandy/mz-transfer-database/blob/master/mz.py#L9),

then run python file (e.g.)

     python mz.py

**It should print:**

    Starting program at [current hour and date]
    
    Procedure of login done


There should be created database file for each sport. It will keep printing till you interrupt program

    Running at [current hour and date]

Intended flow of this program:
1. logging in, without showing any window (separate login for both sports)
2. program changes language of MZ to English
3. downloading data from transfer list
4. parsing data
5. downloading data from training history
6. parsing transfer graph
7. updating data to SQLite3 database (creates new if there is no database)

Program was written for Windows. It should work on Linux systems.
For Ubuntu, I had to change position of [PHPSESID](https://gitlab.com/ReadyBandy/mz-transfer-database/blob/master/logintomz.py#L18)
and [here](https://gitlab.com/ReadyBandy/mz-transfer-database/blob/master/logintomz.py#L36)
in file logintomz.py. Working version is available in [this project](https://gitlab.com/ReadyBandy/mz-transfer-database/tree/master/ubuntu%20server%20login)
Just swap this file with file from main folder in this project.