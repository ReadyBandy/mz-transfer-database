#-*- coding: utf-8 -*- 
from webbot import Browser
import time

def SoccerGetSSID(login, password):
    """function logs in to managerzone.com  soccer using high level lib. webbot (over selenium)"""
    web = Browser(showWindow = False)
    web.go_to('managerzone.com')
    web.type(login) 
    web.type(password, into='Password' , id='passwordFieldId')
    web.press(web.Key.ENTER)
    time.sleep(3)
    #if it says page doesn't exists it's still should work
    web.go_to('https://www.managerzone.com/?p=settings&sub=lang&sport=soccer&lang=en')
    time.sleep(2)
    web.go_to('https://www.managerzone.com/?p=clubhouse&changesport=soccer')
    time.sleep(1)
    cookie=web.get_cookies()
    ssid=cookie[1]['value']
    get='PHPSESSID='+ssid
    web.quit()
    return get

def HockeyGetSSID(login, password):
    """function logs in to managerzone.com hockey using high level lib. webbot (over selenium)"""
    web = Browser(showWindow = False)
    web.go_to('managerzone.com')
    web.type(login) 
    web.type(password, into='Password' , id='passwordFieldId')
    web.press(web.Key.ENTER)
    time.sleep(3)
    #if it says page doesn't exists it's still should work
    web.go_to('https://www.managerzone.com/?p=settings&sub=lang&sport=soccer&lang=en')
    time.sleep(2)
    web.go_to('https://www.managerzone.com/?p=clubhouse&changesport=hockey')
    time.sleep(1)
    cookie=web.get_cookies()
    ssid=cookie[1]['value']
    get='PHPSESSID='+ssid
    web.quit()
    return get


