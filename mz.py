#-*- coding: utf-8 -*- 

import managerzonehockey
import managerzonesoccer
import datetime
import time
import requests

lgin=''   #set your login 'login'
pswd=''   #set your password 'password'
managerzonesoccer.login=lgin
managerzonesoccer.password=pswd
managerzonehockey.login=lgin
managerzonehockey.password=pswd
currentDT = datetime.datetime.now()

#main loop of whole program
while(1):
    try:
        print('Starting program at', (str(currentDT.strftime("%H:%M:%S %d-%m-%Y"))))
        managerzonesoccer.SGetCookie()
        managerzonehockey.HGetCookie()
        print('Procedure of login done')
        while(1):
            managerzonesoccer.SoccerMainFunc()
            managerzonehockey.HockeyMainFunc()
            managerzonesoccer.SoccerMainFunc()
            currentDT = datetime.datetime.now()
            print ('Running at:', str(currentDT.strftime("%H:%M:%S %d-%m-%Y")))
    #still collecting possible erros
    #possible problem with Keyboard Interrupt, need to investigate
    except requests.exceptions.ConnectionError: 
        print('ConnectionError at', str(currentDT.strftime("%H:%M:%S %d-%m-%Y")))
        print('Restarting program...')
        time.sleep(5)
        continue 
    except ValueError: 
        print('Value Error at', str(currentDT.strftime("%H:%M:%S %d-%m-%Y")))
        print('Restarting program...')
        time.sleep(5)
        continue 
    except NameError: 
        print('Name Error at', str(currentDT.strftime("%H:%M:%S %d-%m-%Y")))
        print('Restarting program...')
        time.sleep(5)
        continue 
