#-*- coding: utf-8 -*- 
import sqlite3


db_conn = sqlite3.connect('soccerdatabase.db')

theCursor = db_conn.cursor()

db_conn.execute("CREATE TABLE IF NOT EXISTS Players(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT, PlayerID INTEGER, Date TEXT, DateDM TEXT, TransferSeason INTEGER, DayOfSeason INTEGER, Hour TEXT, TeamID INTEGER, TeamName TEXT, TeamCountry TEXT, Nationality TEXT, NationalTeam INTEGER, IsTrainingGraph INTEGER, CampInThisSeason INTEGER, Age INTEGER, Season INTEGER, TotalBalls INTEGER, Speed INTEGER, SpeedMax INTEGER, Stamina INTEGER, StaminaMax INTEGER, PlayerInt INTEGER, PlayerIntMax INTEGER, Passing INTEGER, PassingMax INTEGER, Shooting INTEGER, ShootingMax INTEGER, Heading INTEGER, HeadingMax INTEGER, Keeping INTEGER, KeepingMax INTEGER, BallControl INTEGER, BallControlMax INTEGER, Tackling INTEGER, TacklingMax INTEGER, AerialPass INTEGER, AerialPassMax INTEGER, SetPlays INTEGER, SetPlaysMax INTEGER, Experience INTEGER, Form INTEGER, Value INTEGER, Salary INTEGER, AskingPrice INTEGER, BuyersTeamName TEXT, BuyersID INTEGER, BuyersCountry TEXT, LatestBid INTEGER);")
db_conn.commit()

def InsertData(player_dictionary):
    """inserting dictionary with information about transfer into database"""
    db_conn.execute("INSERT INTO Players (Name, PlayerID, Date, DateDM, TransferSeason, DayOfSeason, Hour, TeamID, TeamName, TeamCountry, Nationality, NationalTeam, IsTrainingGraph, CampInThisSeason, Age, Season, TotalBalls, Speed, SpeedMax, Stamina, StaminaMax, PlayerInt, PlayerIntMax, Passing, PassingMax, Shooting, ShootingMax, Heading, HeadingMax, Keeping, KeepingMax, BallControl, BallControlMax, Tackling, TacklingMax, AerialPass, AerialPassMax, SetPlays, SetPlaysMax, Experience, Form, Value, Salary, AskingPrice, BuyersTeamName, BuyersID, BuyersCountry, LatestBid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (player_dictionary['Name'], player_dictionary['PlayerID'], player_dictionary['Date'], player_dictionary['DateDM'], player_dictionary['TransferSeason'], player_dictionary['DayOfSeason'], player_dictionary['Hour'], player_dictionary['TeamID'],  player_dictionary['TeamName'],player_dictionary['TeamCountry'], player_dictionary['Nationality'], player_dictionary['NationalTeam'],player_dictionary['IsTrainingGraph'], player_dictionary['CampInThisSeason'], player_dictionary['Age'], player_dictionary['Season'],player_dictionary['TotalBalls'], player_dictionary['Speed'], player_dictionary['SpeedMax'], player_dictionary['Stamina'], player_dictionary['StaminaMax'], player_dictionary['PlayerIntelligence'], player_dictionary['PlayIntMax'], player_dictionary['Passing'], player_dictionary['PassingMax'], player_dictionary['Shooting'], player_dictionary['ShootingMax'], player_dictionary['Heading'], player_dictionary['HeadingMax'], player_dictionary['Keeping'], player_dictionary['KeepingMax'], player_dictionary['BallControl'], player_dictionary['BallControlMax'], player_dictionary['Tackling'], player_dictionary['TacklingMax'], player_dictionary['AerialPass'], player_dictionary['AerialPassMax'], player_dictionary['SetPlays'], player_dictionary['SetPlaysMax'], player_dictionary['Experience'], player_dictionary['Form'], player_dictionary['Value'], player_dictionary['Salary'],player_dictionary['AskingPrice'], player_dictionary['BuyersTeamName'],player_dictionary['BuyersID'],player_dictionary['BuyersCountry'], player_dictionary['LatestBid']))
    db_conn.commit()

def ReadLatestBidfromDB(player_dictionary):
    """reading latest record from db of given player"""
    theCursor = db_conn.cursor()
    PlayerID=int(player_dictionary['PlayerID'])
    theCursor.execute("SELECT LatestBid FROM Players WHERE PlayerID=? ORDER BY ID DESC LIMIT 1", (PlayerID,))
    row=theCursor.fetchall()
    theCursor.close()
    try:
        return row[0][0]
    except:
        return 'New player'

def ReadDateAndHourfromDB(player_dictionary):
    """reading hour+date from db of given player"""
    theCursor = db_conn.cursor()
    PlayerID=int(player_dictionary['PlayerID'])
    theCursor.execute("SELECT Date, Hour, ID FROM Players WHERE PlayerID=? ORDER BY ID DESC LIMIT 1", (PlayerID,))
    row=theCursor.fetchall()
    theCursor.close()
    try:
        DateAndHour=row[0][0]+" "+row[0][1]
        return DateAndHour
    except:
        return player_dictionary['Date']+" "+player_dictionary['Hour']